**Title**:

**Summary**:

**Keywords, themes**:

**Instructions**: 

Want to write about this topic? Hooray! Assign yourself to the issue to claim it.
Create a blog post MR ([see instructions](https://about.gitlab.com/handbook/marketing/blog/#styles-guidelines)). 
Don’t forget to fill in all the relevant frontmatter. When you’re happy with it,
assign to @rebecca to review. Feel free to ping the content team (@rebecca, 
@erica, @evhoffmann, @atflowers, @suripatel) if you have questions or get stuck. 